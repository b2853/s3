package com.zuitt;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Scanner in = new Scanner(System.in);

        try {
            System.out.print("Input an integer whose factorial will be computed: ");
            int num = in.nextInt();

            int answer = 1, counter = 1;

            if (num < 0) {
                System.out.println("Negative numbers are not supported");
                return;
            }

            while (counter <= num) {
                answer *= counter;
                counter++;
            }

            answer = 1;
            counter = 1;
            for (; counter <= num; counter++) {
                answer *= counter;
            }

            System.out.println("The factorial of " + num + " is " + answer);
        } catch (Exception e) {
            System.out.println("Input invalid!");
        }
    }
}
